const express = require("express");
const app = express();
const port = 3000;

const pkg = require("./package.json");

app.get("/", (req, res) =>
  res.json({
    name: pkg.name,
    version: pkg.version,
    description: pkg.description,
    author: pkg.author
  })
);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
